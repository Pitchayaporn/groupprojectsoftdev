/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.model;

import com.mycompany.gojobgroup.dao.CategoryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kim
 */
public class Product {
    private int id;
    private String name;
    private float price;
    private int categoryId;
    private Category category ;

    public Product(int id, String name, float price, String size, int categoryId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.categoryId = categoryId;
    }
    
    public Product(String name, float price, String size,  int categoryId) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.categoryId = categoryId;
    }
    
    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.categoryId = 0;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
     public int getCategoryid() {
        return categoryId;
    }
     
      public void setCategoryid(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", categoryId=" + categoryId + ", category=" + category + '}';
    }



 
    
    
    
    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        
        try {
            product.setId(rs.getInt("PD_ID"));
            product.setName(rs.getString("PD_name"));
            product.setPrice(rs.getFloat("PD_price"));
            product.setCategoryid(rs.getInt("CTGR_ID"));
           CategoryDao cd = new CategoryDao();
           Category category = cd.get(product.getCategoryId());
            product.setCategory(category);
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
