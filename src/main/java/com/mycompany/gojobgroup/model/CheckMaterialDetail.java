/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PT
 */
public class CheckMaterialDetail {
    private int id;
    private int matid;
    private String name;
    private String status;
    private int qty;
    private String dateExp;
    private int min;
    private String unit;
    private Date lastdate;

    public CheckMaterialDetail(int id, int matid, String name, String status, int qty, String dateExp, int min, String unit, Date lastdate) {
        this.id = id;
        this.matid = matid;
        this.name = name;
        this.status = status;
        this.qty = qty;
        this.dateExp = dateExp;
        this.min = min;
        this.unit = unit;
        this.lastdate = lastdate;
    }
    
    public CheckMaterialDetail(int id, int matid, String name, String status, int qty, String dateExp, int min, String unit) {
        this.id = id;
        this.matid = matid;
        this.name = name;
        this.status = status;
        this.qty = qty;
        this.dateExp = dateExp;
        this.min = min;
        this.unit = unit;
    }
    
    public CheckMaterialDetail(int matid, String name, String status, int qty, String dateExp, int min, String unit, Date lastdate) {
        this.id = -1;
        this.matid = matid;
        this.name = name;
        this.status = status;
        this.qty = qty;
        this.dateExp = dateExp;
        this.min = min;
        this.unit = unit;
        this.lastdate = lastdate;
    }
    
    public CheckMaterialDetail(int matid, String name, String status, int qty, String dateExp, int min, String unit) {
        this.id = -1;
        this.matid = matid;
        this.name = name;
        this.status = status;
        this.qty = qty;
        this.dateExp = dateExp;
        this.min = min;
        this.unit = unit;
    }
    
    
    public CheckMaterialDetail() {
        this.id = -1;
        this.matid = 0;
        this.name = "";
        this.status = "OOS";
        this.qty = 0;
        this.dateExp = "";
        this.min = 0;
        this.unit = "";
        this.lastdate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatid() {
        return matid;
    }

    public void setMatid(int matid) {
        this.matid = matid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDateExp() {
        return dateExp;
    }

    public void setDateExp(String dateExp) {
        this.dateExp = dateExp;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Date getLastdate() {
        return lastdate;
    }

    public void setLastdate(Date lastdate) {
        this.lastdate = lastdate;
    }

    @Override
    public String toString() {
        return "CheckMaterialDetail{" + "id=" + id + ", matid=" + matid + ", name=" + name + ", status=" + status + ", qty=" + qty + ", dateExp=" + dateExp + ", min=" + min + ", unit=" + unit + ", lastdate=" + lastdate + '}';
    }

    
    public static CheckMaterialDetail fromRS(ResultSet rs) {
        CheckMaterialDetail checkmaterial = new CheckMaterialDetail();
        try {
            checkmaterial.setId(rs.getInt("MaterailDetail_id"));
            checkmaterial.setMatid(rs.getInt("Mat_Id"));
            checkmaterial.setName(rs.getString("Mat_Name"));
            checkmaterial.setStatus(rs.getString("status"));
            checkmaterial.setQty(rs.getInt("Qty"));
            checkmaterial.setDateExp(rs.getString("date_Exp"));
            checkmaterial.setMin(rs.getInt("Min"));
            checkmaterial.setUnit(rs.getString("Unit"));
            checkmaterial.setLastdate(rs.getDate("last_update"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkmaterial;
    }
    
}