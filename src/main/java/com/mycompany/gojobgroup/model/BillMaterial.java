/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterial {

    private static BillMaterial BillMaterial;
    private int Bill_ID;
    private int Emp_ID;
    private String Vendor_Name;
    private int Bill_Total_Money;
    private int Bill_Received;
    private int Bill_Change;
    private Date Bill_Purchase_Date;
    private Date Bill_Receiving_Date;
    private int Bill_Total_After_Discount;
    private int Bill_Discount;
    
    
    public BillMaterial(int Bill_ID,int Emp_ID,String Vendor_Name,int Bill_Total_Money,int Bill_Received,int Bill_Change,Date Bill_Purchase_Date,Date Bill_Receiving_Date,int Bill_Total_After_Discount,int Bill_Discount) {
        this.Bill_ID = Bill_ID;
        this.Emp_ID = Emp_ID;
        this.Vendor_Name = Vendor_Name;
        this.Bill_Total_Money = Bill_Total_Money;
        this.Bill_Received = Bill_Received;
        this.Bill_Change = Bill_Change;
        this.Bill_Purchase_Date = Bill_Purchase_Date;
        this.Bill_Receiving_Date = Bill_Receiving_Date;
        this.Bill_Total_After_Discount = Bill_Total_After_Discount;
        this.Bill_Discount = Bill_Discount;
    }
    public BillMaterial(int Emp_ID,String Vendor_Name,int Bill_Total_Money,int Bill_Received,int Bill_Change,Date Bill_Purchase_Date,Date Bill_Receiving_Date,int Bill_Total_After_Discount,int Bill_Discount) {
        this.Bill_ID = -1;
        this.Emp_ID = Emp_ID;
        this.Vendor_Name = Vendor_Name;
        this.Bill_Total_Money = Bill_Total_Money;
        this.Bill_Received = Bill_Received;
        this.Bill_Change = Bill_Change;
        this.Bill_Purchase_Date = Bill_Purchase_Date;
        this.Bill_Receiving_Date = Bill_Receiving_Date;
        this.Bill_Total_After_Discount = Bill_Total_After_Discount;
        this.Bill_Discount = Bill_Discount;
    }
    public BillMaterial(String Vendor_Name,int Bill_Total_Money,int Bill_Received,int Bill_Change,Date Bill_Purchase_Date,Date Bill_Receiving_Date,int Bill_Total_After_Discount,int Bill_Discount) {
        this.Bill_ID = -1;
        this.Emp_ID = -1;
        this.Vendor_Name = Vendor_Name;
        this.Bill_Total_Money = Bill_Total_Money;
        this.Bill_Received = Bill_Received;
        this.Bill_Change = Bill_Change;
        this.Bill_Purchase_Date = Bill_Purchase_Date;
        this.Bill_Receiving_Date = Bill_Receiving_Date;
        this.Bill_Total_After_Discount = Bill_Total_After_Discount;
         this.Bill_Discount = Bill_Discount;
    }
    public BillMaterial() {
        this.Bill_ID = -1;
        this.Emp_ID = -1;
        this.Vendor_Name = "";
        this.Bill_Total_Money = 0;
        this.Bill_Received = 0;
        this.Bill_Change = 0;
        this.Bill_Purchase_Date = null;
        this.Bill_Receiving_Date = null;
        this.Bill_Total_After_Discount = 0;
        this.Bill_Discount = 0;
    }

    public int getBill_ID() {
        return Bill_ID;
    }

    public void setBill_ID(int Bill_ID) {
        this.Bill_ID = Bill_ID;
    }

    public int getEmp_ID() {
        return Emp_ID;
    }

    public void setEmp_ID(int Emp_ID) {
        this.Emp_ID = Emp_ID;
    }

    public String getVendor_Name() {
        return Vendor_Name;
    }

    public void setVendor_Name(String Vendor_Name) {
        this.Vendor_Name = Vendor_Name;
    }

    public int getBill_Total_Money() {
        return Bill_Total_Money;
    }

    public void setBill_Total_Money(int Bill_Total_Money) {
        this.Bill_Total_Money = Bill_Total_Money;
    }

    public int getBill_Received() {
        return Bill_Received;
    }

    public void setBill_Received(int Bill_Received) {
        this.Bill_Received = Bill_Received;
    }

    public int getBill_Change() {
        return Bill_Change;
    }

    public void setBill_Change(int Bill_Change) {
        this.Bill_Change = Bill_Change;
    }

    public Date getBill_Purchase_Date() {
        return Bill_Purchase_Date;
    }

    public void setBill_Purchase_Date(Date Bill_Purchase_Date) {
        this.Bill_Purchase_Date = Bill_Purchase_Date;
    }

    public Date getBill_Receiving_Date() {
        return Bill_Receiving_Date;
    }

    public void setBill_Receiving_Date(Date Bill_Receiving_Date) {
        this.Bill_Receiving_Date = Bill_Receiving_Date;
    }

    public int getBill_Total_After_Discount() {
        return Bill_Total_After_Discount;
    }

    public void setBill_Total_After_Discount(int Bill_Total_After_Discount) {
        this.Bill_Total_After_Discount = Bill_Total_After_Discount;
    }
     public int getBill_Discount() {
        return Bill_Discount;
    }

    public void setBill_Discount(int Bill_Discount) {
        this.Bill_Discount = Bill_Discount;
    }
    

    @Override
    public String toString() {
        return "BillMaterial{" + "Bill_ID=" + Bill_ID + ", Emp_ID=" + Emp_ID + ", Vendor_Name=" + Vendor_Name + ", Bill_Total_Money=" + Bill_Total_Money + ", Bill_Received=" + Bill_Received + ", Bill_Change=" + Bill_Change + ", Bill_Purchase_Date=" + Bill_Purchase_Date + ", Bill_Receiving_Date=" + Bill_Receiving_Date + ", Bill_Total_After_Dicount=" + Bill_Total_After_Discount + ", Bill_Discount=" + Bill_Discount + '}';
    }
    
    
 
  public static BillMaterial fromRS(ResultSet rs) {
        BillMaterial billMaterial = new BillMaterial();
        try {
            billMaterial.setBill_ID(rs.getInt("Bill_ID"));
            billMaterial.setEmp_ID(rs.getInt("Emp_ID"));
            billMaterial.setVendor_Name(rs.getString("Vendor_Name"));
            billMaterial.setBill_Total_Money(rs.getInt("Bill_Total_Money"));
            billMaterial.setBill_Received(rs.getInt("Bill_Received"));
            billMaterial.setBill_Change(rs.getInt("Bill_Change"));
            billMaterial.setBill_Purchase_Date(rs.getDate("Bill_Purchase_Date"));
            billMaterial.setBill_Receiving_Date(rs.getDate("Bill_Receiving_Date"));
            billMaterial.setBill_Total_After_Discount(rs.getInt("Bill_Total_After_Discount"));
            billMaterial.setBill_Discount(rs.getInt("Bill_Discount"));
        } catch (SQLException ex) {
            Logger.getLogger(BillMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return BillMaterial;
    }


}
