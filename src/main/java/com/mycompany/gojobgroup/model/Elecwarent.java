/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class Elecwarent {
    private int id;
    private String name;
    private java.util.Date date;
    private double price;
    private String status;

    public Elecwarent(int id, String name, Date date, double price, String status) {
        this.id = id;
        this.name = name;
        this.date =  date;
        this.price = price;
        this.status = status;
    }

    public Elecwarent(String name, Date date, double price, String status) {
        this.id = -1;
        this.name = name;
        this.date = date;
        this.price = price;
        this.status = status;
    }
    
    public Elecwarent(String name, double price, String status) {
        this.id = -1;
        this.name = name;
        this.date = null;
        this.price = price;
        this.status = status;
    }

    public Elecwarent() {
        this.id = -1;
        this.name = "";
        this.date = null;
        this.price = 0;
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Elecwarent{" + "id=" + id + ", name=" + name + ", date=" + date + ", price=" + price + ", status=" + status + '}';
    }

    public static Elecwarent fromRS(ResultSet rs) {
        Elecwarent ewr = new Elecwarent();
        try {
            ewr.setId(rs.getInt("elecwarent_id"));
            ewr.setName(rs.getString("elecwarent_name"));
            ewr.setDate(rs.getTimestamp("elecwarent_date"));
            ewr.setPrice(rs.getDouble("elecwarent_price"));
            ewr.setStatus(rs.getString("elecwarent_status"));
        } catch (SQLException ex) {
            Logger.getLogger(Elecwarent.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ewr;
    }
}
