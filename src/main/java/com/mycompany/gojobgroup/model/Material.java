/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.model;

import java.io.Serializable;

/**
 *
 * @author PT
 */
public class Material implements Serializable{
    private int id;
    private String name;
    private int Min;
    private int QOH;
    private String Unit;
    private double Price;
    private static int lastId = 1;
    public Material(String name, int Min, int QOH, String Unit, double Price) {
        this(Material.lastId++, name, Min, QOH, Unit, Price);
    }

    
    public Material(int id, String name, int Min, int QOH, String Unit, double Price) {
        this.id = id;
        this.name = name;
        this.Min = Min;
        this.QOH = QOH;
        this.Unit = Unit;
        this.Price = Price;
    }    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin() {
        return Min;
    }

    public void setMin(int Min) {
        this.Min = Min;
    }

    public int getQOH() {
        return QOH;
    }

    public void setQOH(int QOH) {
        this.QOH = QOH;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String Unit) {
        this.Unit = Unit;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public static int getLastId() {
        return lastId;
    }

    public static void setLastId(int lastId) {
        Material.lastId = lastId;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", Min=" + Min + ", QOH=" + QOH + ", Unit=" + Unit + ", Price=" + Price + '}';
    }
}
    
