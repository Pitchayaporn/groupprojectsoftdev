/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author PT
 */
public class CheckMaterial {
    private int id;
    private Date date;
    private String empid;
    private String fname;
    private String lname;
    private Date time;
    
    //1
    public CheckMaterial(int id, Date date, String empid, String fname, String lname, Date time) {
        this.id = id;
        this.date = date;
        this.empid = empid;
        this.fname = fname;
        this.lname = lname;
        this.time = time;
    }
    //2
    public CheckMaterial(Date date, String empid, String fname, String lname, Date time) {
        this.id = -1;
        this.date = date;
        this.empid = empid;
        this.fname = fname;
        this.lname = lname;
        this.time = time;
    }
    //3
    public CheckMaterial(String empid, String fname, String lname) {
        this.id = -1;
        this.empid = empid;
        this.fname = fname;
        this.lname = lname;
    }
    //4
    public CheckMaterial() {
        this.id = -1;
        this.date = null;
        this.empid = "";
        this.fname = "";
        this.lname = "";
        this.time = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "CheckMaterial{" + "id=" + id + ", date=" + date + ", empid=" + empid + ", fname=" + fname + ", lname=" + lname + ", time=" + time + '}';
    }
    
    public static CheckMaterial fromRS(ResultSet rs) {
        CheckMaterial checkmaterial = new CheckMaterial();
        try {
            checkmaterial.setId(rs.getInt("CheckMaterial_id"));
            checkmaterial.setDate(rs.getDate("Date"));
            checkmaterial.setEmpid(rs.getString("emp_id"));
            checkmaterial.setFname(rs.getString("emp_fname"));
            checkmaterial.setLname(rs.getString("emp_lname"));
            checkmaterial.setTime(rs.getDate("Time"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkmaterial;
    }
}
