/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


import com.mycompany.gojobgroup.model.Elecwarent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author HP
 */
public class MaterialReport {
    private int id;
    private String name;
    private int min;
    private int qty;

    public MaterialReport(int id, String name, int min, int qty) {
        this.id = id;
        this.name = name;
        this.min = min;
        this.qty = qty;
    }

    public MaterialReport(String name, int min, int qty) {
        this.id = -1;
        this.name = name;
        this.min = min;
        this.qty = qty;
    }

    public MaterialReport() {
        this(-1,"",0,0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "MaterialReport{" + "id=" + id + ", name=" + name + ", min=" + min + ", qty=" + qty + '}';
    }
    
    public static MaterialReport fromRS(ResultSet rs) {
        MaterialReport obj = new MaterialReport();
        try {
            obj.setId(rs.getInt("MaterailDetail_id"));
            obj.setName(rs.getString("Mat_Name"));
            obj.setMin(rs.getInt("Min"));
            obj.setQty(rs.getInt("Qty"));
        } catch (SQLException ex) {
            Logger.getLogger(Elecwarent.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
    
}
