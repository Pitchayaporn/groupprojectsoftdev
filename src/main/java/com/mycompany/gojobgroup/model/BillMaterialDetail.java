/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDetail {

     private static BillMaterialDetail BillMaterialDetail;
    private int Bill_Detail_ID;
    private int Mat_ID;
    private int Bill_ID;
    private int Bill_ID_Quantity;
    private int Bill_ID_Total_Price;
    

    public BillMaterialDetail(int Bill_Detail_ID, int Mat_ID, int Bill_ID, int Bill_ID_Quantity, int Bill_ID_Total_Price) {
        this.Bill_Detail_ID = Bill_Detail_ID;
        this.Mat_ID = Mat_ID;
        this.Bill_ID = Bill_ID;
        this.Bill_ID_Quantity = Bill_ID_Quantity;
        this.Bill_ID_Total_Price = Bill_ID_Total_Price;
    }
    public BillMaterialDetail(int Mat_ID, int Bill_ID, int Bill_ID_Quantity, int Bill_ID_Total_Price) {
        this.Bill_Detail_ID = -1;
        this.Mat_ID = -1;
        this.Bill_ID = Bill_ID;
        this.Bill_ID_Quantity = Bill_ID_Quantity;
        this.Bill_ID_Total_Price = Bill_ID_Total_Price;
    }
     public BillMaterialDetail(int Bill_ID, int Bill_ID_Quantity, int Bill_ID_Total_Price) {
        this.Bill_Detail_ID = -1;
        this.Mat_ID = -1;
        this.Bill_ID = -1;
        this.Bill_ID_Quantity = Bill_ID_Quantity;
        this.Bill_ID_Total_Price = Bill_ID_Total_Price;
    }
     public BillMaterialDetail() {
        this.Bill_Detail_ID = -1;
        this.Mat_ID = -1;
        this.Bill_ID = -1;
        this.Bill_ID_Quantity = 0;
        this.Bill_ID_Total_Price = 0;
    }

    public static BillMaterialDetail getBillMaterialDetail() {
        return BillMaterialDetail;
    }

    public static void setBillMaterialDetail(BillMaterialDetail BillMaterialDetail) {
        BillMaterialDetail.BillMaterialDetail = BillMaterialDetail;
    }

    public int getBill_Detail_ID() {
        return Bill_Detail_ID;
    }

    public void setBill_Detail_ID(int Bill_Detail_ID) {
        this.Bill_Detail_ID = Bill_Detail_ID;
    }

    public int getMat_ID() {
        return Mat_ID;
    }

    public void setMat_ID(int Mat_ID) {
        this.Mat_ID = Mat_ID;
    }

    public int getBill_ID() {
        return Bill_ID;
    }

    public void setBill_ID(int Bill_ID) {
        this.Bill_ID = Bill_ID;
    }

    public int getBill_ID_Quantity() {
        return Bill_ID_Quantity;
    }

    public void setBill_ID_Quantity(int Bill_ID_Quantity) {
        this.Bill_ID_Quantity = Bill_ID_Quantity;
    }
    public int getBill_ID_Total_Price() {
        return Bill_ID_Total_Price;
    }

    public void setBill_ID_Total_Price(int Bill_ID_Total_Price) {
        this.Bill_ID_Total_Price = Bill_ID_Total_Price;
    }

     @Override
    public String toString() {
        return "BillMaterialDetail{" + "Bill_Detail_ID=" + Bill_Detail_ID + ", Mat_ID=" + Mat_ID +
                ", Bill_ID=" + Bill_ID + ", Bill_ID_Quantity=" + Bill_ID_Quantity + ", Bill_ID_Total_Price=" + Bill_ID_Total_Price + '}';
    }

 
  public static BillMaterialDetail fromRS(ResultSet rs) {
        BillMaterialDetail billMaterialDetail= new BillMaterialDetail();
        try {
            billMaterialDetail.setBill_Detail_ID(rs.getInt("Bill_Detail_ID"));
            billMaterialDetail.setMat_ID(rs.getInt("Mat_ID"));
            billMaterialDetail.setBill_ID(rs.getInt("Bill_ID"));
            billMaterialDetail.setBill_ID_Quantity(rs.getInt("Bill_ID_Quantity"));
            billMaterialDetail.setBill_ID_Total_Price(rs.getInt("Bill_ID_Total_Price"));
           
        } catch (SQLException ex) {
            Logger.getLogger(BillMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return BillMaterialDetail;
    }
}
