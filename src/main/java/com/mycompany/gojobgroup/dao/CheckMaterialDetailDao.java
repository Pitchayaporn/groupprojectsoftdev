/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.dao;

import com.mycompany.gojobgroup.helper.DatabaseHelper;
import com.mycompany.gojobgroup.model.CheckMaterialDetail;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PT
 */
public class CheckMaterialDetailDao implements Dao<CheckMaterialDetail>{

    @Override
    public CheckMaterialDetail get(int id) {
        CheckMaterialDetail checkmaterialdetail = null;
        String sql = "SELECT * FROM CHECKMATERIALDETAILS WHERE MaterailDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                checkmaterialdetail = new CheckMaterialDetail();
                checkmaterialdetail.setId(rs.getInt("MaterailDetail_id"));
                checkmaterialdetail.setMatid(rs.getInt("Mat_Id"));
                checkmaterialdetail.setName(rs.getString("Mat_Name"));
                checkmaterialdetail.setStatus(rs.getString("status"));
                checkmaterialdetail.setQty(rs.getInt("Qty"));
                checkmaterialdetail.setDateExp(rs.getString("date_Exp"));
                checkmaterialdetail.setMin(rs.getInt("Min"));
                checkmaterialdetail.setUnit(rs.getString("Unit"));
                checkmaterialdetail.setLastdate(rs.getDate("last_update"));
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkmaterialdetail;
    }

    @Override
    public List<CheckMaterialDetail> getAll() {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKMATERIALDETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                CheckMaterialDetail checkmaterialdetail = new CheckMaterialDetail();
                checkmaterialdetail.setId(rs.getInt("MaterailDetail_id"));
                checkmaterialdetail.setMatid(rs.getInt("Mat_Id"));
                checkmaterialdetail.setName(rs.getString("Mat_Name"));
                checkmaterialdetail.setStatus(rs.getString("status"));
                checkmaterialdetail.setQty(rs.getInt("Qty"));
                checkmaterialdetail.setDateExp(rs.getString("date_Exp"));
                checkmaterialdetail.setMin(rs.getInt("Min"));
                checkmaterialdetail.setUnit(rs.getString("Unit"));
                checkmaterialdetail.setLastdate(rs.getDate("last_update"));
                
                list.add(checkmaterialdetail);
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<CheckMaterialDetail> getAll(String order) {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECKMATERIALDETAILS ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialDetail checkmaterialdetail = CheckMaterialDetail.fromRS(rs);
                list.add(checkmaterialdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override //fine test 
    public CheckMaterialDetail save(CheckMaterialDetail obj) {
        String sql = "INSERT INTO CHECKMATERIALDETAILS (MaterailDetail_id, Mat_Id, Mat_Name, status, Qty, date_Exp, Min, Unit)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            stmt.setInt(2,obj.getMatid());
            stmt.setString(3,obj.getName());
            stmt.setString(4,obj.getStatus());
            stmt.setInt(5,obj.getQty());
            stmt.setString(6,obj.getDateExp());
            stmt.setInt(7,obj.getMin());
            stmt.setString(8,obj.getUnit());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override //fine test 
    public CheckMaterialDetail update(CheckMaterialDetail obj) {
        String sql = "UPDATE CHECKMATERIALDETAILS"
                    + " SET Mat_Id = ?, Mat_Name = ?, status = ?, Qty = ?, date_Exp = ?, Min = ?, Unit = ?"
                    + " WHERE MaterailDetail_id = ?";
            Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getMatid());
            stmt.setString(2,obj.getName());
            stmt.setString(3,obj.getStatus());
            stmt.setInt(4,obj.getQty());
            stmt.setString(5,obj.getDateExp());
            stmt.setInt(6,obj.getMin());
            stmt.setString(7,obj.getUnit());
            stmt.setInt(8,obj.getId());
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override //fine test 
    public int delete(CheckMaterialDetail obj) {
        String sql = "DELETE FROM CHECKMATERIALDETAILS WHERE MaterailDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }
    
}
