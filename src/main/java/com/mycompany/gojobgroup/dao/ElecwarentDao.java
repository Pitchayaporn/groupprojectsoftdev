/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.dao;


import com.mycompany.gojobgroup.helper.DatabaseHelper;
import com.mycompany.gojobgroup.model.Elecwarent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class ElecwarentDao implements Dao<Elecwarent> {

    @Override
    public Elecwarent get(int id) {
        Elecwarent elecwarent = null;
        String sql = "SELECT * FROM ELECWARENT WHERE elecwarent_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                elecwarent = Elecwarent.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return elecwarent;
    }

    @Override
    public List<Elecwarent> getAll() {
        ArrayList<Elecwarent> list = new ArrayList();
        String sql = "SELECT * FROM ELECWARENT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Elecwarent elecwarent = Elecwarent.fromRS(rs);
                elecwarent.setId(rs.getInt("elecwarent_id"));
                elecwarent.setName(rs.getString("elecwarent_name"));
                elecwarent.setDate(rs.getDate("elecwarent_date"));
                elecwarent.setPrice(rs.getDouble("elecwarent_price"));
                elecwarent.setStatus(rs.getString("elecwarent_status"));

                list.add(elecwarent);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Elecwarent save(Elecwarent obj) {
        String sql = "INSERT INTO ELECWARENT (elecwarent_name, elecwarent_date, elecwarent_price, elecwarent_status)"
                + " VALUES (?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setLong(2, obj.getDate().getTime()); // Use getTime() to store the timestamp
            stmt.setDouble(3, obj.getPrice());
            stmt.setString(4, obj.getStatus());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Elecwarent update(Elecwarent obj) {
        String sql = "UPDATE ELECWARENT"
                + " SET elecwarent_name = ?, elecwarent_date = ?, elecwarent_price = ?, elecwarent_status = ?"
                + " WHERE elecwarent_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setLong(2, obj.getDate().getTime()); // Use getTime() to store the timestamp
            stmt.setDouble(3, obj.getPrice());
            stmt.setString(4, obj.getStatus());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Elecwarent obj) {
        String sql = "DELETE FROM ELECWARENT WHERE elecwarent_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Elecwarent> getAll(String order) {
        ArrayList<Elecwarent> list = new ArrayList();
        String sql = "SELECT * FROM ELECWARENT ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Elecwarent elecwarent = Elecwarent.fromRS(rs);
                list.add(elecwarent);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Elecwarent> getAll(String where, String order) {
        ArrayList<Elecwarent> list = new ArrayList();
        String sql = "SELECT * FROM ELECWARENT where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Elecwarent elecwarent = Elecwarent.fromRS(rs);
                list.add(elecwarent);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
