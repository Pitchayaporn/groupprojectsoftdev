/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.dao;

import com.mycompany.gojobgroup.helper.DatabaseHelper;
import com.mycompany.gojobgroup.model.BillMaterial;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDao implements Dao<BillMaterial> {

    @Override
    public BillMaterial get(int id) {
        BillMaterial billMaterial = null;
        String sql = "SELECT * FROM  BILLMATERIAL WHERE Bill_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billMaterial = BillMaterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billMaterial;
    }

    public BillMaterial getDate(Date date) {
        BillMaterial billMaterial = null;
        String sql = "SELECT * FROM BILLMATERIAL WHERE Bill_Purchase_Date=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDate(1, date);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                billMaterial = BillMaterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billMaterial;
    }
    

    @Override
    public List<BillMaterial> getAll() {
        ArrayList<BillMaterial> list = new ArrayList();
        String sql = "SELECT * FROM BILLMATERIAL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterial billMaterial = BillMaterial.fromRS(rs);
                billMaterial.setBill_ID(rs.getInt("Bill_ID"));
                billMaterial.setEmp_ID(rs.getInt("Emp_ID"));
                billMaterial.setVendor_Name(rs.getString("Vendor_Name"));
                billMaterial.setBill_Total_Money(rs.getInt("Bill_Total_Money"));
                billMaterial.setBill_Received(rs.getInt("Bill_Received"));
                billMaterial.setBill_Change(rs.getInt("Bill_Change"));
                billMaterial.setBill_Purchase_Date(rs.getDate("Bill_Purchase_Date"));
                billMaterial.setBill_Receiving_Date(rs.getDate("Bill_Receiving_Date"));
                billMaterial.setBill_Total_After_Discount(rs.getInt("Bill_Total_After_Discount"));
                billMaterial.setBill_Discount(rs.getInt("Bill_Discount"));
                
                list.add(billMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    public List<BillMaterial> getAll(String where, String order) {
        ArrayList<BillMaterial> list = new ArrayList();
        String sql = "SELECT * FROM BILLMATERIAL where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterial billMaterial = BillMaterial.fromRS(rs);
                list.add(billMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillMaterial> getAll(String order) {
        ArrayList<BillMaterial> list = new ArrayList();
        String sql = "SELECT * FROM BILLMATERIAL  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterial billMaterial = BillMaterial.fromRS(rs);
                list.add(billMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillMaterial save(BillMaterial obj) {
        String sql = "INSERT INTO BILLMATERIAL (Vendor_Name , Bill_Total_Money, Bill_Received, Bill_Change, Bill_Purchase_Date, Bill_Receiving_Date, Bill_Total_After_Discount ,Bill_Discount)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getVendor_Name());
            stmt.setInt(2, obj.getBill_Total_Money());
            stmt.setInt(3, obj.getBill_Received());
            stmt.setInt(4, obj.getBill_Change());
            stmt.setDate(5, (Date) obj.getBill_Purchase_Date());
            stmt.setDate(6, (Date) obj.getBill_Receiving_Date());
            stmt.setInt(7, obj.getBill_Total_After_Discount());
            stmt.setInt(8, obj.getBill_Discount());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setBill_ID(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillMaterial update(BillMaterial obj) {
        String sql = "UPDATE BILLMATERIAL"
                + " SET Bill_Total_Money = ?, Bill_Received = ?, Bill_Change = ? , Bill_Purchase_Date = ?,Bill_Receiving_Date = ?, Bill_Total_After_Discount = ?,Bill_Discount = ?"
                + " WHERE Bill_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_Total_Money());
            stmt.setInt(2, obj.getBill_Received());
            stmt.setInt(3, obj.getBill_Change());
            stmt.setDate(4, (Date) obj.getBill_Purchase_Date());
            stmt.setDate(5, (Date) obj.getBill_Receiving_Date());
            stmt.setInt(6, obj.getBill_Total_After_Discount());
            stmt.setInt(7, obj.getBill_Discount());
            stmt.setInt(8, obj.getBill_ID());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillMaterial obj) {
        String sql = "DELETE FROM BILLMATERIAL WHERE Bill_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_ID());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
