/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.dao;

import com.mycompany.gojobgroup.helper.DatabaseHelper;
import com.mycompany.gojobgroup.model.BillMaterialDetail;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDetailDao {
     public BillMaterialDetail get(int id) {
        BillMaterialDetail billMaterialdetail = null;
        String sql = "SELECT * FROM  BIIMATERIALDETAIL WHERE Bill_Detail_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billMaterialdetail = BillMaterialDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billMaterialdetail;
    }

   
    public List<BillMaterialDetail> getAll() {
        ArrayList<BillMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM BIIMATERIALDETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterialDetail billMaterialdetail = BillMaterialDetail.fromRS(rs);
                list.add(billMaterialdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    
    }
     public List<BillMaterialDetail> getAll(String where, String order) {
        ArrayList<BillMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM BIIMATERIALDETAIL where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterialDetail billMaterialdetail = BillMaterialDetail.fromRS(rs);
                list.add(billMaterialdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
   
    public List<BillMaterialDetail> getAll(String order) {
        ArrayList<BillMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM BIIMATERIALDETAIL  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterialDetail billMaterialdetail = BillMaterialDetail.fromRS(rs);
                list.add(billMaterialdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    
    public BillMaterialDetail save(BillMaterialDetail obj) {
         String sql = "INSERT INTO BIIMATERIALDETAIL (int Bill_Detail_ID,int Mat_ID,int Bill_ID,int Bill_ID_Quantity,int Bill_ID_Total_Price)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_Detail_ID());
            stmt.setInt(2, obj.getMat_ID());
            stmt.setInt(3, obj.getBill_ID());
            stmt.setInt(4, obj.getBill_ID_Quantity());
            stmt.setInt(5, obj.getBill_ID_Total_Price());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setBill_Detail_ID(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    
    public BillMaterialDetail update(BillMaterialDetail obj) {
         String sql = "UPDATE BIIMATERIAL"
                + " SET Bill_Detail_ID = ?, Mat_ID = ?, Bill_ID = ? , Bill_ID_Quantity = ?,Bill_ID_Total_Price = ? "
                + " WHERE Bill_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_Detail_ID());
            stmt.setInt(2, obj.getMat_ID());
            stmt.setInt(3, obj.getBill_ID());
            stmt.setInt(4, obj.getBill_ID_Quantity());
            stmt.setInt(5, obj.getBill_ID_Total_Price());
            
       
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    
    public int delete(BillMaterialDetail obj) {
        String sql = "DELETE FROM BIIMATERIALDETAIL WHERE Bill_Detail_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_ID());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
}
