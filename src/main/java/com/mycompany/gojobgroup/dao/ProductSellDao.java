/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */



import com.mycompany.gojobgroup.dao.Dao;
import com.mycompany.gojobgroup.helper.DatabaseHelper;
import com.mycompany.stockmanagement.model.PeoductSell;
import com.mycompany.stockmanagement.model.ProductSellReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class ProductSellDao implements Dao<PeoductSell> {

    @Override
    public PeoductSell get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<PeoductSell> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public PeoductSell save(PeoductSell obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public PeoductSell update(PeoductSell obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(PeoductSell obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<PeoductSell> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ProductSellReport> getProductSellByTotalPrice() {
        ArrayList<ProductSellReport> list = new ArrayList();
        String sql = """
                     SELECT PD_ID, PD_name, SUM(RCD_Quantity) AS TotalQuantity,
                              SUM(RCD_PPU * RCD_Quantity) AS TotalPrice
                     FROM RECIEPTDETAILS RCD NATURAL JOIN PRODUCT
                     --INNER JOIN PRODUCT PD ON PD.PD_ID=RCD.PD_ID
                     GROUP BY PD_ID
                     ORDER BY TotalPrice DESC
                     LIMIT 10;
                            """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductSellReport obj = ProductSellReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
