/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.dao;

import com.mycompany.gojobgroup.helper.DatabaseHelper;
import com.mycompany.gojobgroup.model.CheckMaterial;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PT
 */
public class CheckMaterialDao implements Dao<CheckMaterial> {

    @Override
    public CheckMaterial get(int id) {
        CheckMaterial checkmaterial = null;
        String sql = "SELECT * FROM CHECKMATERIAL WHERE MaterailDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                checkmaterial = new CheckMaterial();
                checkmaterial.setId(rs.getInt("CheckMaterial_id"));
                checkmaterial.setDate(rs.getDate("Date"));
                checkmaterial.setEmpid(rs.getString("emp_id"));
                checkmaterial.setFname(rs.getString("emp_fname"));
                checkmaterial.setLname(rs.getString("emp_lname"));
                checkmaterial.setTime(rs.getDate("Time"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkmaterial;
    }

    @Override
    public List<CheckMaterial> getAll() {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM CHECKMATERIAL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                CheckMaterial checkmaterial = new CheckMaterial();
                checkmaterial.setId(rs.getInt("CheckMaterial_id"));
                checkmaterial.setDate(rs.getDate("Date"));
                checkmaterial.setEmpid(rs.getString("emp_id"));
                checkmaterial.setFname(rs.getString("emp_fname"));
                checkmaterial.setLname(rs.getString("emp_lname"));
                checkmaterial.setTime(rs.getDate("Time"));
                
                list.add(checkmaterial);
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

        public List<CheckMaterial> getAll(String order) {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM CHECKMATERIAL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial checkmaterial = CheckMaterial.fromRS(rs);
                list.add(checkmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
        
        
    @Override
    public CheckMaterial save(CheckMaterial obj) {
        String sql = "INSERT INTO CHECKMATERIAL (CheckMaterial_id, emp_id, emp_fname, emp_lname)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            stmt.setString(2,obj.getEmpid());
            stmt.setString(3,obj.getFname());
            stmt.setString(4,obj.getLname());
            stmt.setDate(2, (Date) obj.getDate());
            stmt.setString(3,obj.getEmpid());
            stmt.setString(4,obj.getFname());
            stmt.setString(5,obj.getLname());
            stmt.setTime(6, (Time) obj.getTime());
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckMaterial update(CheckMaterial obj) {
        String sql = "UPDATE CHECKMATERIAL"
                    + " SET emp_id = ?, emp_fname = ?, emp_lname = ?"
                    + " WHERE CheckMaterial_id = ?";
            Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getEmpid());
            stmt.setString(2,obj.getFname());
            stmt.setString(3,obj.getLname());
            stmt.setInt(4,obj.getId());
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckMaterial obj) {
        String sql = "DELETE FROM CHECKMATERIAL WHERE CheckMaterial_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
}
