/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;


import com.mycompany.gojobgroup.dao.CheckInOutDao;
import com.mycompany.gojobgroup.model.CheckInOut;
import java.util.List;

/**
 *
 * @author pasinee
 */
public class CheckInOutService {
    
    public CheckInOut getById(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.get(id);
    }

    public List<CheckInOut> getCheckInOuts(){
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAll(" check_in_out_id asc");
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.save(editedCheckInOut);
    }

    //
    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.delete(editedCheckInOut);
    }
    
    
}
