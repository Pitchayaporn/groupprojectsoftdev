/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;

import com.mycompany.gojobgroup.dao.CheckMaterialDao;
import com.mycompany.gojobgroup.model.CheckMaterial;
import java.util.List;

/**
 *
 * @author PT
 */
public class CheckMaterialService {
    public List<CheckMaterial> getCheckMaterial(){
        CheckMaterialDao  checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.getAll(" CheckMaterial_id asc");
    }
    
    public CheckMaterial addNew(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.save(editedCheckMaterial);
    }

    public CheckMaterial update(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.update(editedCheckMaterial);
    }

    public int delete(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.delete(editedCheckMaterial);
    }
}
