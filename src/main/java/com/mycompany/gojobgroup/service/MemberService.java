/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;

import com.mycompany.gojobgroup.dao.MemberDao;
import com.mycompany.gojobgroup.model.Member;
import java.util.List;

/**
 *
 * @author Arthaphan
 */
public class MemberService {
    public Member getByPhone(String phone) {
        MemberDao memberDao = new MemberDao();
        Member member = memberDao.getByTel(phone);
        return member;
    }
    
    public List<Member> getCategories(){
        MemberDao memberDao = new MemberDao();
        return memberDao.getAll(" Mem_ID ASC");
    }

    public Member addNew(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.save(editedMember);
    }

    public Member update(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.update(editedMember);
    }

    public int delete(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.delete(editedMember);
    }
    
}
