/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


import com.mycompany.stockmanagement.model.ProductSellReport;
import java.util.List;


/**
 *
 * @author HP
 */
public class ProductSellService {
    public List<ProductSellReport> getProductSellByTotalPrice() {
        ProductSellDao productSellDao = new ProductSellDao();
        return productSellDao.getProductSellByTotalPrice();
    }
}
