/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;

import com.mycompany.gojobgroup.dao.BillMaterialDao;
import com.mycompany.gojobgroup.model.BillMaterial;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialService {

//     private BillMaterial editedBillMaterial;
//     private Date date;
//   

     public BillMaterial getById(int id) {
        BillMaterialDao billMaterialDao = new BillMaterialDao();
        return billMaterialDao.get(id);
    }


    public List<BillMaterial> getBillMaterial() {
        BillMaterialDao billMaterialDao = new BillMaterialDao();
        return billMaterialDao.getAll(" Vendor_Name asc");
    }
    public BillMaterial getByPur(Date date) {
        BillMaterialDao billMaterialDao = new BillMaterialDao();
        return billMaterialDao.getDate(date);
        
    }
   
    public BillMaterial addNew(BillMaterial editedBillMaterial) {
        BillMaterialDao billMaterialDao = new BillMaterialDao();
        return billMaterialDao.save(editedBillMaterial);
    }

    public BillMaterial update(BillMaterial editedBillMaterial) {
        BillMaterialDao billMaterialDao = new BillMaterialDao();
        return billMaterialDao.update(editedBillMaterial);
    }

    public int delete(BillMaterial editedBillMaterial) {
        BillMaterialDao billMaterialDao = new BillMaterialDao();
        return billMaterialDao.delete(editedBillMaterial);
    }
    
    
    
}
