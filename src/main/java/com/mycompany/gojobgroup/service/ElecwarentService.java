/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;


import com.mycompany.gojobgroup.dao.ElecwarentDao;
import com.mycompany.gojobgroup.model.Elecwarent;
import java.util.List;

/**
 *
 * @author HP
 */
public class ElecwarentService {
    public Elecwarent getById(int id) {
        ElecwarentDao elecwarentDao = new ElecwarentDao();
        return elecwarentDao.get(id);
    }
    
    public List<Elecwarent> getElecwarents(){
        ElecwarentDao elecwarentDao = new ElecwarentDao();
        return elecwarentDao.getAll(" elecwarent_name asc");
    }

    public Elecwarent addNew(Elecwarent editedElecwarent) {
        ElecwarentDao elecwarentDao = new ElecwarentDao();
        return elecwarentDao.save(editedElecwarent);
    }

    public Elecwarent update(Elecwarent editedElecwarent) {
        ElecwarentDao elecwarentDao = new ElecwarentDao();
        return elecwarentDao.update(editedElecwarent);
    }

    public int delete(Elecwarent editedElecwarent) {
        ElecwarentDao elecwarentDao = new ElecwarentDao();
        return elecwarentDao.delete(editedElecwarent);
    }
}
