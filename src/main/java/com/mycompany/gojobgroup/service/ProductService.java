/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;
import com.mycompany.gojobgroup.dao.ProductDao;
import com.mycompany.gojobgroup.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kim
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName(){
        return (ArrayList<Product>) productDao.getAll(" PD_name ASC ");
    }
     public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" PD_ID asc");
    }

    public Product addNew(Product editedUser) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedUser);
    }

    public Product update(Product editedUser) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedUser);
    }

    public int delete(Product editedUser) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedUser);
    }
}
