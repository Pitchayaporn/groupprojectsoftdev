/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;


import com.mycompany.gojobgroup.dao.CategoryDao;
import com.mycompany.gojobgroup.model.Category;
import java.util.List;

/**
 *
 * @author werapan
 */
public class CategoryService {
    public Category getByID(int id) {
        CategoryDao categoryDao = new CategoryDao();
        Category category = categoryDao.get(id);
        return category;

    }
    
    public Category getByType(String type) {
        CategoryDao categoryDao = new CategoryDao();
        Category category = categoryDao.getByType(type);
        return category;

    }
    
    public List<Category> getCategories(){
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll(" CTGR_ID ASC");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }
}
