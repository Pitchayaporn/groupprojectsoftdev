/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;

import com.mycompany.gojobgroup.dao.BillMaterialDetailDao;
import com.mycompany.gojobgroup.model.BillMaterialDetail;
import java.util.List;


/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDetailService {
     private BillMaterialDetail editedBillMaterialDetail;
     public BillMaterialDetail getById(int id) {
        BillMaterialDetailDao BillMaterialDetailDao = new BillMaterialDetailDao();
        return BillMaterialDetailDao.get(id);
    }

    public List<BillMaterialDetail> getBillMaterialDetail() {
        BillMaterialDetailDao billMaterialDetailDao = new BillMaterialDetailDao();
        return billMaterialDetailDao.getAll(" Bill_Detail_ID asc");
    }
   
        

    public BillMaterialDetail addNew(BillMaterialDetail editedPromotion) {
        BillMaterialDetailDao billMaterialDetailDao = new BillMaterialDetailDao();
        return billMaterialDetailDao.save(editedBillMaterialDetail);
    }

    public BillMaterialDetail update(BillMaterialDetail editedBillMaterialDetail) {
        BillMaterialDetailDao billMaterialDetailDao = new BillMaterialDetailDao();
        return billMaterialDetailDao.update(editedBillMaterialDetail);
    }

    public int delete(BillMaterialDetail editedBillMaterialDetail) {
        BillMaterialDetailDao billMaterialDetailDao = new BillMaterialDetailDao();
        return billMaterialDetailDao.delete(editedBillMaterialDetail);
    }
    
}
