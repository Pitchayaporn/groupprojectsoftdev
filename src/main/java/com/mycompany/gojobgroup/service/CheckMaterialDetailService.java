/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.service;

import com.mycompany.gojobgroup.dao.CheckMaterialDetailDao;
import com.mycompany.gojobgroup.model.CheckMaterialDetail;
import java.util.List;

/**
 *
 * @author PT
 */
public class CheckMaterialDetailService {
    
    public List<CheckMaterialDetail> getCheckMaterial(){
        CheckMaterialDetailDao  checkmaterialdetaildao = new CheckMaterialDetailDao();
        return checkmaterialdetaildao.getAll(" MaterailDetail_id asc");
    }
    
    public CheckMaterialDetail addNew(CheckMaterialDetail editedCheckMaterial) {
        CheckMaterialDetailDao checkmaterialdetaildao = new CheckMaterialDetailDao();
        return checkmaterialdetaildao.save(editedCheckMaterial);
    }

    public CheckMaterialDetail update(CheckMaterialDetail editedCheckMaterial) {
        CheckMaterialDetailDao checkmaterialdetaildao = new CheckMaterialDetailDao();
        return checkmaterialdetaildao.update(editedCheckMaterial);
    }

    public int delete(CheckMaterialDetail editedCheckMaterial) {
        CheckMaterialDetailDao checkmaterialdetaildao = new CheckMaterialDetailDao();
        return checkmaterialdetaildao.delete(editedCheckMaterial);
    }
}
