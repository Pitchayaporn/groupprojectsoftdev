/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gojobgroup.data;

import com.mycompany.gojobgroup.dao.EmployeeDao;
import com.mycompany.gojobgroup.model.Employee;


/**
 *
 * @author Admin
 */
public class TestEmployeeDao {

    public static void main(String[] args) {
        EmployeeDao employeeDao = new EmployeeDao();
        for(Employee e: employeeDao.getAll()){
            System.out.println(e);
        }
    }
}
