import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JComboBoxExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("JComboBox Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String[] items = {"Option 1", "Option 2", "Option 3", "Option 4"};

        JComboBox<String> comboBox = new JComboBox<>(items);
        comboBox.setSelectedIndex(0); // Set the initial selected item

        JButton button = new JButton("Get Selected Name");

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedName = (String) comboBox.getSelectedItem();
                JOptionPane.showMessageDialog(frame, "Selected Name: " + selectedName);
            }
        });

        JPanel panel = new JPanel();
        panel.add(comboBox);
        panel.add(button);

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}
